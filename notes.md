
# Notes

### Run Python Tests
`python -m unittest -v pack_py/__init__.py`

### Rust Tests & Debugging
`cargo test`
`rust-gdb target/debug/deps/pack_rs-34e488c135935d43`

### Cross Compilation Issue
Resolved by adding linker target to driver-rs/.cargo/config
```toml
[target.armv7-unknown-linux-gnueabihf]
linker = "arm-linux-gnueabihf-gcc"
```


