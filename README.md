N!%PaaS
=======

## Overview

In an effort to diversify our quantum offerings, we have decided to begin offering __N!%PaaS__ or,
for those that aren't up-to-date with the hottest new tech jargon,
__factorial of n under modulo p as a service__.

The service works as follows:

The customer submits a job, with 3 parameters (all unsigned 32-bit integers):

* `packet id`
    An ID to identify the job
* `n`
    The number to calculate the factorial of under modulo p
* `p`
    The modulo under which factorial(n) is calculated

The job is then sent over a proprietary QuantumBus® transport to the host.

The host then unpacks the buffer, calculates `n! % p` and returns a buffer with the following
information (again all unsigned 32-bit integers):

* `packet id`
    The packet id that was received in the job buffer
* `result`
    The result of `n! % p`

The result is then sent over the QuantumBus® transport back to the client.

## Design Parameters

### Python Client

The Python client must be fully cross-platform (though it will be tested on an x64 workstation).

The client must be able to run on Python `3.8` without any external dependencies.

The developer must complete the following function declarations in `pack_py/__init__.py`.

```python
def pack(pack_id: int, n: int, p: int) -> bytes :
    """
    Pack N!%P packet into buffer

    Parameters:
        pack_id (unsigned int): Packet ID
        n (unsigned int): number to calculate factorial of under modulo `p`
        p (unsigned int): modulo under which to calculate `n`!

    Returns:
        buffer of packed bytes

    Raises:
        ValueError if any parameter is outside of limits
    """
    pass

def unpack(buff: bytes) -> (int, int) :
    """
    Unpack N!%P response buffer

    Parameters:
        buff (bytes): Buffer to unpack

    Returns:
        a tuple of form (<id>, <result>)
    """
    pass
```

### Rust Host

The Rust host must also be fully cross-platform (though it will be tested on a
32-bit ARM processor with hard float).

__! Important Constraints !__

* the `n! % p` calculation must not overflow for `n < 1000`
* unit tests are strongly encouraged
* extra dependencies may be added, but they must be appropriate for a production environment
* extra points given if the codebase can operate in a `no_std` environment without `alloc`

The developer must complete the following function declaration in `pack-rs/src/lib.rs`:

```rust
/// Error Processing N!%P Packet
#[derive(Debug,Copy,Clone)]
pub enum ProcessError {
}

/// Process an N!%P packet
///
/// The N!%P packet is received from the `rx_buff` buffer, unpacked, and processed.
///
/// The calculated result is then packed and written to `tx_buff`.
///
/// On success, `Ok<usize>` is returned where `usize` is the number of bytes written to
/// `tx_buff`.
///
/// On failure, `Err<ProcessError>` is returned.
pub fn process(_rx_buff : &[u8], _tx_buff: &mut [u8]) -> Result<usize,ProcessError> {
    Ok(0)
}
```

## Testing

To run the test server, you will need to have `qemu` installed.

For Debian-based operating systems, install with:

```sh
sudo apt install qemu-user
```

You must also have the `armv7-unknown-linux-gnueabihf` toolchain installed.

```sh
rustup target add armv7-unknown-linux-gnueabihf
```

### Start the emulated ARM Rust server

```sh
./run-arm.sh
```

### Run native Python test fixture

```sh
./run-tests.sh
```
