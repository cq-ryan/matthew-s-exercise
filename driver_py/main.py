import socket
from pack_py import pack, unpack
import inspect

HOST = "127.0.0.1"
PORT = 9001
BUFFER_SIZE = 1024


def line_number():
    """
    Helper function to get line number of current program
    """
    return inspect.currentframe().f_back.f_lineno


def transfer_bytes(tx_buff: bytes) -> bytes:
    """
    Transmit and receive bytes over TCP socket
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((HOST, PORT))
        sock.send(tx_buff)
        data = sock.recv(BUFFER_SIZE)

    return data


def n_fac_p(pack_id: int, n: int, p: int) -> int:
    """
    Get N!%P result from remote server
    """
    buff = pack(pack_id, n, p)
    res_buff = transfer_bytes(buff)
    ret_pack_id, result = unpack(res_buff)
    assert (
        ret_pack_id == pack_id
    ), "Expected transmitted packet ID to equal received packet ID"
    return result


def factorial(n):
    """
    Recursive factorial implementation
    """
    if n == 0 or n == 1:
        return 1
    return factorial(n - 1) * n


def n_fac_p_test(n, p, pack_id=line_number()):
    """
    Test N!%P against local implementation
    """
    print(f"Testing {n:4}! % {p:4} (packet id {pack_id:3})")
    assert n_fac_p(pack_id, n, p) == factorial(n) % p


if __name__ == "__main__":
    # Test some bad inputs
    try:
        n_fac_p(-35, 42, 100)
        n_fac_p(35, -42, 100)
        n_fac_p(35, 42, -100)
        raise AssertionError("Expected ValueError from bad input!")
    except ValueError:
        pass

    n_fac_p_test(1, 13, line_number())
    n_fac_p_test(25, 41, line_number())
    n_fac_p_test(32, 76, line_number())
    n_fac_p_test(22, 4, line_number())
    n_fac_p_test(100, 137, line_number())
    n_fac_p_test(925, 1004, line_number())

    import random

    NUM_ITERATIONS = 1000

    for _ in range(NUM_ITERATIONS):
        n = random.randrange(0, 925)
        p = random.randrange(n + 1, 950)
        pack_id = random.randrange(1, 10000)
        n_fac_p_test(n, p, pack_id)
