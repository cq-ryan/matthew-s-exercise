#![no_std]

// Parameters are 32-bit integers
const PARAM_SIZE_BYTES: usize = 4;

/// Error Processing N!%P Packet
#[derive(Debug, Copy, Clone)]
pub enum ProcessError {
    TooLargeN,
}


/// Decode u32 from a little-endian encoded array of u8 bytes
///
/// On success, `Ok<u32> is returned where `u32` is the decoded integer
///
/// On failure, `Err<ProcessError` is returned
fn read_u32_from_bytes_little(_input_byte_array: &[u8]) -> Result<u32, ProcessError> {
    Ok (
        ((_input_byte_array[0] as u32) <<  0) +
        ((_input_byte_array[1] as u32) <<  8) +
        ((_input_byte_array[2] as u32) << 16) +
        ((_input_byte_array[3] as u32) << 24)
    )
}

/// Encode u32 to a little-endian encoded array of u8 bytes
///
/// On success, `Ok<u32> is returned where `u32` is the number of bytes written to the buffer
///
/// On failure, `Err<ProcessError>` is returned
fn write_bytes_from_u32_little(_input_u32: &u32 , _output_byte_array: &mut [u8]) -> Result<u32, ProcessError> {
    _output_byte_array[0] =  ((_input_u32 >>  0) & 0xff) as u8;
    _output_byte_array[1] =  ((_input_u32 >>  8) & 0xff) as u8;
    _output_byte_array[2] =  ((_input_u32 >> 16) & 0xff) as u8;
    _output_byte_array[3] =  ((_input_u32 >> 24) & 0xff) as u8;
    Ok (4)
}

/// Calculate N!%P for N < 1000
///
/// On success, `Ok<u32> is returned where `u32` is the result
///
/// On failure, `Err<ProcessError>` is returned
fn calculate_nfact_mod_p(n: &u32, p: &u32) -> Result<u32, ProcessError> {
    match (*n, *p) {
        // Input Validation
        (n, ..) if n >= 1000 => Err(ProcessError::TooLargeN),

        // If P is in N!, the result will always be 0
        (n, p)  if n >= p => Ok(0),

        (n, p) => {
            // Note: Result should never be larger than P, so it can be stored in 4 bytes
            let mut _result:u32 = 1;

            // A straight calculation of n! would overflow
            // Instead calculate incrementaly, utilizing modulo's distributive property
            for _i in 2..=n {
                _result = (_result * _i) % p;
            }
            Ok(_result)
        },
    }
}


/// Process an N!%P packet
///
/// The N!%P packet is received from the `rx_buff` buffer, unpacked, and processed.
///
/// The calculated result is then packed and written to `tx_buff`.
///
/// On success, `Ok<usize>` is returned where `usize` is the number of bytes written to
/// `tx_buff`.
///
/// On failure, `Err<ProcessError>` is returned.
pub fn process(_rx_buff: &[u8], _tx_buff: &mut [u8]) -> Result<usize, ProcessError> {

    let _n: u32 = read_u32_from_bytes_little(&_rx_buff[PARAM_SIZE_BYTES..2*PARAM_SIZE_BYTES])?;
    let _p: u32 = read_u32_from_bytes_little(&_rx_buff[2*PARAM_SIZE_BYTES..3*PARAM_SIZE_BYTES])?;

    let mut _result:u32 = calculate_nfact_mod_p(&_n, &_p)?;

    // Encode ID
    _tx_buff[0..PARAM_SIZE_BYTES].clone_from_slice(&_rx_buff[0..PARAM_SIZE_BYTES]);

    // Encode result
    write_bytes_from_u32_little(&_result, &mut _tx_buff[PARAM_SIZE_BYTES..2*PARAM_SIZE_BYTES])?;

    // Function always writes 8 bytes to tx buffer
    Ok(2*PARAM_SIZE_BYTES)
}

#[cfg(test)]
mod tests {
    // Put any unit tests that you would like to run here
    #[test]
    fn test_pack() {

        // --- Test u32 Decoding ---
        fn u32_decode_eq(expected_val: u32 , input_buf: &[u8]) {
            let result = crate::read_u32_from_bytes_little(&input_buf);
            assert_eq!(expected_val, result.unwrap());
        }

        u32_decode_eq(0,    &[0x00, 0x00, 0x00, 0x00]);
        u32_decode_eq(1,    &[0x01, 0x00, 0x00, 0x00]);
        u32_decode_eq(22,   &[0x16, 0x00, 0x00, 0x00]);
        u32_decode_eq(11,   &[0x0B, 0x00, 0x00, 0x00]);
        u32_decode_eq(31,   &[0x1F, 0x00, 0x00, 0x00]);
        u32_decode_eq(999,  &[0xE7, 0x03, 0x00, 0x00]);
        u32_decode_eq(1001, &[0xE9, 0x03, 0x00, 0x00]);
        u32_decode_eq(4278190080,   &[0x00, 0x00, 0x00, 0xFF]);
        u32_decode_eq(4294967295,   &[0xFF, 0xFF, 0xFF, 0xFF]);


        // --- Test u32 Encoding ---

        fn u32_encode_eq(input_val_to_encode: u32 , expected_buf : &[u8]) {
            let mut actual_buf: [u8; 4] = [0x00; 4];
            crate::write_bytes_from_u32_little(&input_val_to_encode, &mut actual_buf)
                .expect("Error writing bytes to buffer");
            assert_eq!(expected_buf, actual_buf);
        }

        u32_encode_eq(0,    &[0x00, 0x00, 0x00, 0x00]);
        u32_encode_eq(1,    &[0x01, 0x00, 0x00, 0x00]);
        u32_encode_eq(22,   &[0x16, 0x00, 0x00, 0x00]);
        u32_encode_eq(11,   &[0x0B, 0x00, 0x00, 0x00]);
        u32_encode_eq(31,   &[0x1F, 0x00, 0x00, 0x00]);
        u32_encode_eq(999,  &[0xE7, 0x03, 0x00, 0x00]);
        u32_encode_eq(1001, &[0xE9, 0x03, 0x00, 0x00]);
        u32_encode_eq(4278190080,   &[0x00, 0x00, 0x00, 0xFF]);
        u32_encode_eq(4294967295,   &[0xFF, 0xFF, 0xFF, 0xFF]);

        // --- Test N!%P calculation

        fn test_calc(expected_result: u32, _n: u32, _p: u32) {
            let result = crate::calculate_nfact_mod_p(&_n, &_p);
            assert_eq!(expected_result, result.unwrap());
        }

        test_calc(0, 1, 1);

        // 3! % 4 == 6 % 4 == 2
        test_calc(2, 3, 4);

        // 11! % 31 == 39916800 % 31 == 22
        test_calc(22, 11, 31);

        // Should not overflow for n < 1000
        // 999! % 31 == 39916800 % 31 == 22
        test_calc(0, 999, 1001);


        // --- Test core N!%P processing

        fn test_process(input_rx_buff: &[u8], expected_tx_buff : &[u8]) {
            let mut actual_tx_buff: [u8; 8] = [0x00; 8];
            crate::process(input_rx_buff, &mut actual_tx_buff)
                .expect("Error processing input");
            assert_eq!(expected_tx_buff, actual_tx_buff);
        }

        test_process(
            &[  0x00, 0x00, 0x00, 0x00,     // ID:  0
                0x00, 0x00, 0x00, 0x00,     // N:   0
                0x00, 0x00, 0x00, 0x00],    // P:   0

            &[  0x00, 0x00, 0x00, 0x00,     // ID:      0
                0x00, 0x00, 0x00, 0x00]     // Result:  0
        );

        test_process(
            &[  0x01, 0x00, 0x00, 0x00,     // ID:  1
                0x0B, 0x00, 0x00, 0x00,     // N:   11
                0x1F, 0x00, 0x00, 0x00],    // P:   31

            &[  0x01, 0x00, 0x00, 0x00,     // ID:      1
                0x16, 0x00, 0x00, 0x00]     // Result:  22
        );

        test_process(
            &[  0xFF, 0xFF, 0xFF, 0xFF,     // ID:  4294967295
                0xE7, 0x03, 0x00, 0x00,     // N:   999
                0xE9, 0x03, 0x00, 0x00],    // P:   1001

            &[  0xFF, 0xFF, 0xFF, 0xFF,     // ID:      4294967295
                0x00, 0x00, 0x00, 0x00]     // Result:  0
        );
    }
}
