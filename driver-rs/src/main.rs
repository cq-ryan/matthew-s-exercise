use std::io::{Read, Write};
use std::net::{Shutdown, TcpListener, TcpStream};
use std::thread;

use pack_rs::process;

fn handle_client(mut stream: TcpStream) {
    let mut rx_data = [0_u8; 100];
    let mut tx_data = [0_u8; 100];

    match stream.read(&mut rx_data) {
        Ok(rx_size) => match process(&rx_data[..rx_size], &mut tx_data[..]) {
            Ok(tx_size) => {
                stream.write_all(&tx_data[0..tx_size]).unwrap();
            }
            Err(e) => {
                println!("Error from process function {:?}", e);
            }
        },
        Err(_) => {
            println!(
                "An error occurred, terminating connection with {}",
                stream.peer_addr().unwrap()
            );
            stream.shutdown(Shutdown::Both).unwrap();
        }
    }
}

fn main() {
    let listener = TcpListener::bind("127.0.0.1:9001").unwrap();

    println!("Listening on {}", listener.local_addr().unwrap());

    for stream in listener.incoming() {
        match stream {
            Ok(new_stream) => {
                thread::spawn(move || {
                    handle_client(new_stream);
                });
            }
            Err(e) => {
                println!("Error: {}", e);
            }
        }
    }
    drop(listener);
}
