import unittest


def pack(pack_id: int, n: int, p: int) -> bytes:
    """
    Pack N!%P packet into buffer

    Parameters:
        pack_id (unsigned int): Packet ID
        n (unsigned int): number to calculate factorial of under modulo `p`
        p (unsigned int): modulo under which to calculate `n`!

    Returns:
        buffer of packed bytes

    Raises:
        ValueError if any parameter is outside of limits
    """

    # Parameters are all unsigned 32-bit integers

    params_to_encode = [ pack_id, n, p ]

    # Input Validation
    if pack_id < 0:
        raise ValueError("Input pack_id=%d is negative" %pack_id)

    if n < 0:
        raise ValueError("Input N=%d is negative" %n)

    if p < 0:
        raise ValueError("Input P=%d is negative" %p)

    # Encode to little-endian, 4 bytes
    buff = bytearray(0)
    for param in params_to_encode:
        buff += param.to_bytes(4, byteorder='little', signed = False)

    return buff


def decode_u32_from_bytes_little(buff: bytes) -> (int):
    """
    Decode a 32-bit int from a little-endian buffer

    Parameters:
        buff (bytes): Buffer to decode

    Returns:
        a 32-bit integer <result>
    """
    return( (buff[0] << 0 ) +
            (buff[1] << 8 ) +
            (buff[2] << 16) +
            (buff[3] << 24) )


def unpack(buff: bytes) -> (int, int):
    """
    Unpack N!%P response buffer

    Parameters:
        buff (bytes): Buffer to unpack

    Returns:
        a tuple of form (<id>, <result>)
    """
    id      = decode_u32_from_bytes_little(buff[0:4])
    result  = decode_u32_from_bytes_little(buff[4:8])
    return (id, result)

class PackTest(unittest.TestCase):
    """
    Place any tests you would like to implement here
    """

    def test_pack(self):

        self.assertEqual(pack(1, 2, 3), b'\x01\x00\x00\x00\x02\x00\x00\x00\x03\x00\x00\x00')

        # Max Value
        self.assertEqual(pack(4294967295, 4294967295, 4294967295), b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF')

        # N < 0
        with self.assertRaises(ValueError):
            pack(0, -1, 0)

        # ID < 0
        with self.assertRaises(ValueError):
            pack(-1, 0, 0)

        # P < 0
        with self.assertRaises(ValueError):
            pack(0, 0, -1)


    def test_unpack(self):

        self.assertEqual(unpack(b'\x00\x00\x00\x00\x00\x00\x00\x00'), (0, 0))

        self.assertEqual(unpack(b'\x01\x00\x00\x00\x02\x00\x00\x00'), (1, 2))

        self.assertEqual(unpack(b'\x00\x00\x00\xff\x01\x00\x00\x00'), (4278190080, 1))



