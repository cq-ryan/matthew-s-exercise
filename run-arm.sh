#!/usr/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" &> /dev/null && pwd  )"

DRIVER_RS_DIR="$SCRIPT_DIR/driver-rs"
DRIVER_BIN_DIR="$DRIVER_RS_DIR/target/armv7-unknown-linux-gnueabihf/debug"
DRIVER_BIN=$DRIVER_BIN_DIR/driver-rs

# Build packages
cd $DRIVER_RS_DIR && cargo build --target=armv7-unknown-linux-gnueabihf

# Run with QEMU
export QEMU_LD_PREFIX=/usr/arm-linux-gnueabihf
qemu-arm $DRIVER_BIN
